﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class soundtest : MonoBehaviour {
	[SerializeField]  List <AudioClip> soundList = new List <AudioClip>();
	[SerializeField] AudioSource source;
	AudioClip sound;

	void playSound () {
		source.clip = sound;
		source.Play ();

	}
}
